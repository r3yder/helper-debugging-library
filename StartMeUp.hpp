//
//  StartMeUp.hpp
//  CVM Projecter
//
//  Created by Marek K on 07.03.2018.
//  Copyright © 2018 Marek K. All rights reserved.
//

#ifndef StartMeUp_hpp
#define StartMeUp_hpp

#include <stdio.h>

// OFFSETS Updated
#define MAIN_ADDR 0x10000a7f2
#define STRLEN_LB_ADDR 0x1005a2818
#define STRLEN_STUB_ADDR 0x10045a886
#define NS_OBJ_LOAD_ADDR 0xc4131
#define BZERO_STUB_ADDR 0x10045a328
#define BZERO_LB_ADDR 0x1005a20f0
#define ARC4_LB_ADDR 0x1005a21c8

#define PATCH_NR1_RETURN_STACK 0x10036f9aa
#define PATCH_NR2_RETURN_STACK 0x10036fa5b
#define PATCH_NR3_RETURN_STACK 0x1002ff252
#define PATCH_ARC4_RETURN_STACK 0x100455bfc

#define PATCHES_JMP 0x10045aa98							// __stub_helper

#define MAGICK_SHIT 0x36f9aa


// Shim Offsets
#define ARC4_STUB_ADDR 0x43caa0
#define PATCH_ARC4_RETURN_STACK 0x10043a0f5

#endif /* StartMeUp_hpp */
