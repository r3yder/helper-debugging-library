//
//  CVMStuff.hpp
//  UnL0ck
//
//  Created by Marek K on 21/02/2033.
//  Copyright © 2033 OleOle. All rights reserved.
//

#ifndef CVMStuff_hpp
#define CVMStuff_hpp

#include <set>
#include <tuple>                // add tuples
#include <string>               // add string
#include <bitset>               // add bitmask
#include <vector>               // add vectors
#include <mach/mach.h>          // mach_task_self_
#include <mach-o/dyld.h>        // _dyld_get_image_header
#include <mach/mach_vm.h>       // mach_vm_region_recurse()
#include <mach-o/getsect.h>     // getsectbynamefromheader_64
#include <iostream>				// For cout. endl
#include <iomanip>

#include "Utilities.hpp"
//#include <experimental/optional>
//#include <experimental/vector>

using namespace std; // it's fine for now

//typedef tuple<string, string, string, mach_vm_address_t, uint64_t, uint8_t, intptr_t> sections;       // Name,segment, section, address, size, bitmask of section, ASLR
//typedef tuple<string, string, string> patch_image_vars;       // Name,segment, section

// Don't use typedf use using
using sections = tuple<string, string, string, mach_vm_address_t, uint64_t, uint8_t, intptr_t>;
using patch_image_vars = tuple<string, string, string>;


/* ######################
 ######### MACROS #######
 ##################### */
#define SLAP_STACK_FRAME	asm("pop %rbp");
#define PUSH_REGISTERS		asm("push %%rdi;" "push %%rsi;" "push %%rdx;" "push %%rcx;" "push %%rbx;" "push %%r8;" "push %%r9;" "push %%r10;" "push %%r11;" "push %%r12;" "push %%r13;" "push %%r14;" ::);
#define POP_REGISTERS		asm("pop %%r14;" "pop %%r13;" "pop %%r12;" "pop %%r11;" "pop %%r10;" "pop %%r9;" "pop %%r8;" "pop %%rbx;" "pop %%rcx;" "pop %%rdx;" "pop %%rsi;" "pop %%rdi;" ::);






struct Section {
	string			name;		// Name of the section
	uintptr_t		addr;		// Start addr of the section
	uint64_t		size;		// Size of section
};

struct Segment {
	string			name;			// Name of the segment
	uintptr_t		addr;			// Start addr of the segment
	uint64_t		size;			// Size of segment
	vector<Section> sections;
};

struct ImageInfo {
	ImageInfo(string a, uintptr_t d, intptr_t s, vector<Segment> ss) : name(a), addr(d), aslr(s), segments(ss) {}
	string						name;		// Name of the image
	uintptr_t					addr;		// Start addr of binary
	intptr_t					aslr;		// ASLR slide of the image
	vector<Segment>				segments;
};


/*
	Here is struct that stores Patch information.
*/
struct Patch {
	string 						id;						// Identifier for patch
	string						data_to_find;			// Pattern to find: 43AA****BD (not yet wildcard)
	string						data_to_pach;			// Patch with data: 4433221111
	patch_image_vars			image_info_data;		// Image name, Section, Segment to look at, example: IDA, __TEXT, __text
	vector<mach_vm_address_t>	addresses;				// Addresses with found pattern: (default empty)
};


struct Shim;
struct jumper;


class CVMStuff {
public:
	CVMStuff();
	
	
	
	// v2.0 Confirmed
	uint16_t searchForPatches();
	uint16_t searchForShims();
	void lookForSections();
	bool addPatch(string id, string find, string replace, patch_image_vars image_info);
	bool addShim(string id, string image, uintptr_t offsetOfTarget, uintptr_t pAddressOfShim);
	void addBinaryInfo(patch_image_vars image_info);
	Shim* getShimWithId(string name);
	static uint64_t patchAddressWithJMP(uintptr_t addressOfTarget, uintptr_t addressOfShim);
	static uint64_t patchAddressWithShimAddress(uintptr_t addressOfTarget, uintptr_t addressOfShim);
	static vm_prot_t setProtection(uintptr_t address, size_t size, vm_prot_t prot);
private:
	template <class C, typename T>
	C* getObjectByName(T object, string name);														// Generic function to get Obj from ImageInfo tree
	tuple<uintptr_t, uint64_t> getAddressFromImage(patch_image_vars image_info_vars);				// This function will fetch address of segment/secttion from proper image
	ImageInfo* addImageInfo(patch_image_vars image_info_vars);
	Segment* addSegmentInfo(ImageInfo* image, patch_image_vars image_info_vars);
	Section* addSectionInfo(Segment* segment, string name);
public:


	void addJMP(uint64_t* pAddressOfTarget, uint64_t* pAddressOfShim, string name);
	void addSectionInfo(string name, string segment, string section);
	void patchData();
	
	/* static methods */
	static uint64_t getASLRaddress(uint64_t addr);
	// static uint64_t patchAddressWithJMP(uint64_t* pAddressOfTarget, uint64_t* pAddressOfShim, bool assembly = true);
	// static vm_prot_t setProtection(const void *address, size_t size, vm_prot_t prot);
	
	jumper* getJMPWithName(string name);
	
	
	
protected:
#if defined (VM_REGION_SUBMAP_SHORT_INFO_COUNT_64)
	typedef vm_region_submap_short_info_data_64_t RegionInfo;
	enum { kRegionInfoSize = VM_REGION_SUBMAP_SHORT_INFO_COUNT_64 };
#else
	typedef vm_region_submap_info_data_64_t RegionInfo;
	enum { kRegionInfoSize = VM_REGION_SUBMAP_INFO_COUNT_64 };
#endif

	
private:
	void clearImageInfoData();
	
	// Needed v1.0
	vector<sections>						binary_sections;			// necessary binary sections found by DYLIB
	
	
	
	
	
	// Needed v2.0
	vector<Shim>							shims;						// Object with shim data
	vector<Patch>							patches;					// Object with patch data
	vector<ImageInfo>						images_info;				// Binary images information
public:
	vector<jumper>							data_to_jmp;				// Object with patch data
};



/*
	Here is struct that stores Shim information.
*/
struct Shim {
	string 						id;						// Identifier for shim
	string						image_name;
	uintptr_t					offsetOfTarget;
	uintptr_t					addressOfShim;
	uintptr_t					addressOfImage;
	uint64_t					datOrigCode;			// Oryginal code 64-bit only support ofc
	
	void enable() {
		cout << YELLOW << "[DEBUG] Enabling SHIM with ID = " << id << " :: Target address = " << hex << addressOfImage + offsetOfTarget << " :: Shim address = " << hex << addressOfShim << RESET << endl;
		datOrigCode = CVMStuff::patchAddressWithJMP(addressOfImage + offsetOfTarget, addressOfShim);
	}
	
	void disable() {
		*(uintptr_t*)(addressOfImage + offsetOfTarget) = datOrigCode;
	}
};




/*
 Here is struct that stores our jmps info
 */
struct jumper {
	string                      name;
	bool                        enabled;
	uint64_t*                   pAddressOfTarget;
	uint64_t*                   pAddressOfShim;
	uint64_t                    datOrigCode;
	
	void tap(bool assembly = true) {
		if (enabled) {
			// Protection is broken in Big Sur version. Cannot by bypassed.
			// vm_prot_t current_prot = CVMStuff::setProtection((mach_vm_address_t*)pAddressOfTarget, sizeof(uint64_t), VM_PROT_ALL);
			// vm_prot_t current_prot3 = CVMStuff::setProtection((mach_vm_address_t*)pAddressOfTarget, sizeof(uint64_t), VM_PROT_ALL);
			*pAddressOfTarget = datOrigCode;
			// CVMStuff::setProtection((mach_vm_address_t*)pAddressOfTarget, sizeof(uint64_t), current_prot);
		} else {
			// datOrigCode = CVMStuff::patchAddressWithJMP(pAddressOfTarget, pAddressOfShim, assembly);
			enabled = true;
		}
		
	}
};



#endif /* CVMStuff_hpp */
