//
//  CVMStuff.cpp
//  CVM Projecter
//
//  Created by Marek K on 21/02/2033.
//  Copyright © 2033 OleOle. All rights reserved.
//

#include "CVMStuff.hpp"

#include <unistd.h>
#include <sys/mman.h>

CVMStuff::CVMStuff() {}

template <class C, typename T>
C* CVMStuff::getObjectByName(T object, string name) {
	auto found = find_if(object->begin(), object->end(), [name](const auto& str) { return name.find(str.name) != string::npos; });

	if (found == object->end()) {
		#if DEBUG
		cout << "[DEBUG] Searching for object with name: " << name << " failed. It needs to be added." << endl;
		#endif
		return nullptr;
	}

	#if DEBUG
	cout << "[DEBUG] Object with name: " << name << " already present. No need to add." << endl;
	#endif
	
	decltype(&*found) ptr;
	ptr = &*found;
	
	return ptr;
}

void CVMStuff::clearImageInfoData() {
	#if DEBUG
	printf("[DEBUG] Clearing image info data TO DO\n");
	#endif
	
	//images_info.clear();
}


void CVMStuff::lookForSections() {
	#if DEBUG
	cout << YELLOW << "[DEBUG] Start searching for image names, segment, sections info addresses..." << RESET << endl;
	#endif
	
	
	uint32_t count = _dyld_image_count();
	
	for (uint32_t i = 0; i < count; i++) {
		string image_name(_dyld_get_image_name(i));
		
		auto found = find_if(images_info.begin(), images_info.end(), [image_name](const auto& str) { return image_name.find(str.name) != string::npos; });
		
		if (found == images_info.end())
			continue;
		
		#if DEBUG
		cout << "[DEBUG] Found image info with name " << image_name << endl;
		#endif

		auto &[name, addr, aslr, segments] = *found;
		
		auto header_addr = (uintptr_t)_dyld_get_image_header(i);
		addr = header_addr;
		
		#if DEBUG
		cout << "[DEBUG] Assign HEADER address: " << showbase << hex << header_addr << endl;
		#endif
		
		aslr = _dyld_get_image_vmaddr_slide(i);
		
		#if DEBUG
		cout << "[DEBUG] Number of segments to found in image: " << segments.size() << endl;
		#endif
		
		for (Segment &segments_data: segments) {
			auto& [name, addr, size, sections ] = segments_data;

			unsigned long size_segment;
			const struct mach_header_64 *mh = (const struct mach_header_64*)_dyld_get_image_header(i);
			auto seg_addr = getsegmentdata(mh, name.c_str(), &size_segment);
			
			#if DEBUG
			cout << "[DEBUG] Found Segment ( " << name << " ) for image ( " << image_name << " ) at :: ( " << hex << (uintptr_t)seg_addr << " ), size ( " << size_segment << " )" << endl;
			#endif
						
			if (seg_addr == NULL) {
				#if DEBUG
				cout << "[DEBUG] Segment not found " << name << endl;
				#endif
				continue;
			}
			
			#if DEBUG
			cout << "[DEBUG] Adding information about segment " << name << " in ( " << image_name << " )" << endl;
			#endif
			
			addr = (uintptr_t)seg_addr;
			size = size_segment;
			
			for (Section &section_data: sections) {
				auto& [sec_name, addr, size ] = section_data;
				
				const struct mach_header_64 *mh = (const struct mach_header_64*)_dyld_get_image_header(i);
				const struct section_64 *section_o = getsectbynamefromheader_64(mh, name.c_str(), sec_name.c_str());

				if (section_o == NULL) {
					#if DEBUG
					cout << "[DEBUG] Section not found " << name << endl;
					#endif
					continue;
				}
				
				#if DEBUG
				cout << "[DEBUG] Adding information about section " << sec_name << " in ( " << image_name << " )" << endl;
				#endif
				
				addr = section_o->addr + aslr;
				size = section_o->size;
				
			}
		}
	}
}

void CVMStuff::addBinaryInfo(patch_image_vars image_info_vars) {
	auto& [image_name, segment_name, section_name] = image_info_vars;

	auto image_obj = addImageInfo(image_info_vars);
	
	if (image_obj != nullptr && !segment_name.empty()) {
		auto segment_obj = addSegmentInfo(image_obj, image_info_vars);

		if (segment_obj != nullptr && !section_name.empty()) {
			addSectionInfo(segment_obj, section_name);
		}
	}
}

ImageInfo* CVMStuff::addImageInfo(patch_image_vars image_info_vars) {
	auto& [image_name, segment_name, section_name] = image_info_vars;
	
	auto obj_image = getObjectByName<ImageInfo>(&images_info, image_name);

	if (obj_image == nullptr) {
		#if DEBUG
		cout << "[DEBUG] Successfully added new ImageInfo section with name: " << image_name << endl;
		#endif

		images_info.push_back( {image_name, NULL, 0, { Segment{segment_name, NULL, 0, { Section{section_name, NULL, 0}  }}   } });
		return nullptr;
	}

	return obj_image;
}

Segment* CVMStuff::addSegmentInfo(ImageInfo* image, patch_image_vars image_info_vars) {
	auto& [image_name, segment_name, section_name] = image_info_vars;
	
	auto obj_segment = getObjectByName<Segment>(&image->segments, segment_name);
	
	if (obj_segment == nullptr) {
		#if DEBUG
		cout << "[DEBUG] Successfully added new Segment section with name: " << segment_name << endl;
		#endif
		image->segments.push_back( { segment_name, NULL, 0, { Section{section_name, NULL, 0}  }} );
		return nullptr;
	}

	return obj_segment;
}

Section* CVMStuff::addSectionInfo(Segment* segment, string name) {
	auto obj_section = getObjectByName<Section>(&segment->sections, name);

	if (obj_section == nullptr) {
		#if DEBUG
		cout << "[DEBUG] Successfully added new Section section with name: " << name << endl;
		#endif
		segment->sections.push_back( {name, NULL, 0} );
		return nullptr;
	}

	return obj_section;
}

bool CVMStuff::addPatch(string id, string find, string replace, patch_image_vars image_info_vars) {
	if (sizeof(find) != sizeof(replace))
		return false;
	
	#if DEBUG
	cout << MAGENTA << "[DEBUG] Adding patch ( size: " << sizeof(find) / 3 << " bytes ), id: ( " << id << " )" << RESET << endl;
	#endif
	
	addBinaryInfo(image_info_vars);

	patches.push_back( {id, find, replace, { image_info_vars }, {} });

	#if DEBUG
	cout << "[DEBUG] Successfully added patch with ID = " << id << endl;
	#endif

	return true;
}

tuple<uintptr_t, uint64_t> CVMStuff::getAddressFromImage(patch_image_vars image_info_vars) {
	auto& [image_name, segment_name, section_name] = image_info_vars;
	
	#if DEBUG
	cout << "[DEBUG] Fetching Image info Obj ( " << image_name << " ) for patch search..." << endl;
	#endif

	auto obj_image = getObjectByName<ImageInfo>(&images_info, image_name);
	

	if (obj_image == nullptr) {
		#if DEBUG
		cout << "[DEBUG] Image Info Obj not found !" << endl;
		#endif

		return {};
	}

	#if DEBUG
	cout << "[DEBUG] Image Info Obj ( " << obj_image->name << " ) found !" << endl;
	#endif
	
	if (segment_name.empty()) {
		#if DEBUG
		cout << "[DEBUG] Returning address ( " << hex << obj_image->addr << " ) :: aslr ( " << hex << obj_image->aslr << " ) for Image Info Obj (HEADER) because segment name information is not defined." << endl;
		#endif
		
		return {obj_image->addr, 0};
	}
	
	#if DEBUG
	cout << "[DEBUG] Fetching Segment Obj ( " << segment_name << " ) for patch search...  " << endl;
	#endif

	auto obj_segment = getObjectByName<Segment>(&obj_image->segments, segment_name);

	if (obj_segment == nullptr) {
		#if DEBUG
		cout << "[DEBUG] Segment Obj not found !" << endl;
		#endif

		return {};
	}
	
	#if DEBUG
	cout << "[DEBUG] Segment Obj ( " << obj_segment->name << " ) found !" << endl;
	#endif
	
	if (section_name.empty()) {
		#if DEBUG
		cout << "[DEBUG] No section info returning Segment data" << endl;
		#endif
		return {obj_segment->addr, obj_segment->size};
	}

	#if DEBUG
	cout << "[DEBUG] Fetching Section Obj ( " << section_name << " ) for patch search..." << endl;
	#endif

	auto obj_section = getObjectByName<Section>(&obj_segment->sections, section_name);

	if (obj_section == nullptr) {
		#if DEBUG
		cout << "[DEBUG] Section not found !" << endl;
		#endif

		return {};
	}
	
	#if DEBUG
	cout << "[DEBUG] Section Obj ( " << obj_section->name << " ) found !" << endl;
	#endif
	
	return {obj_section->addr, obj_section->size};
}

uint16_t CVMStuff::searchForPatches() {
	#if DEBUG
	cout << CYAN << "[DEBUG] Start searching for patches......." << RESET << endl;
	#endif
	
	uint16_t count = 0;
	for (Patch &patch: patches) {
		#if DEBUG
		cout << "[DEBUG] Getting start address search for patch with ID = " << patch.id << endl;
		#endif

		auto [start_addr, size] = getAddressFromImage(patch.image_info_data);

				
		if (!start_addr || !size) {
			#if DEBUG
			cout << RED << "[DEBUG] Address or size for patch not defined/found." << RESET << endl;
			#endif

			continue;
		}
		
		#if DEBUG
		cout << "[DEBUG] Searching at ( " << hex << start_addr << " ), size ( " << size << " ), pattern ( " << patch.data_to_find << " ), for patch with ID = " << patch.id << endl;
		#endif
		
		size_t n = 0;
		string_view sv{(char*)start_addr, size};
		
		while (sv.npos != (n = sv.find(patch.data_to_find, n))) {
			mach_vm_address_t addr = (mach_vm_address_t)(start_addr + n);
			patch.addresses.emplace_back(addr);
			++n;
			++count;

			#if DEBUG
			cout << GREEN << "[DEBUG] Found new addres to patch at == " << hex << addr << RESET << endl;
			#endif
		}

	}
	
	return count;
}


bool CVMStuff::addShim(string id, string image, uintptr_t offsetOfTarget, uintptr_t pAddressOfShim) {
	#if DEBUG
	cout << CYAN << "[DEBUG] Start adding Shim data = " << id << RESET << endl;
	#endif

	auto image_vars = patch_image_vars(image, "", "");
	addBinaryInfo(image_vars);
	
	shims.push_back({id, image, offsetOfTarget, pAddressOfShim });

	#if DEBUG
		printf("[DEBUG] Added Shim data = %s\n", id.c_str());
	#endif
	
	return true;
}

uint16_t CVMStuff::searchForShims() {
	#if DEBUG
	cout << CYAN << "[DEBUG] Start searching for shims addresses......." << RESET << endl;
	#endif
	
	uint16_t count = 0;
	for (Shim &shim: shims) {
		#if DEBUG
		cout << "[DEBUG] Getting start address for Shim with ID = " << shim.id << endl;
		#endif

		auto image_vars = patch_image_vars(shim.image_name, "", "");
		auto [start_addr, size] = getAddressFromImage(image_vars);
				
		if (!start_addr) {
			#if DEBUG
			cout << "[DEBUG] Address for shim not defined/found." << endl;
			#endif

			continue;
		}
		
		#if DEBUG
		cout << "[DEBUG] Found start address for Shim with ID = " << shim.id << " :: " << hex << start_addr + shim.offsetOfTarget << " offset = " << shim.offsetOfTarget << endl;
		#endif
		
		shim.addressOfImage = start_addr;
	}
	
	return count;
}

Shim* CVMStuff::getShimWithId(string name) {
	for(auto& obj : shims) {
		if (obj.id == name) {
			#if DEBUG
			cout << "[DEBUG] Returning Shim Object with name = " << name << endl;
			#endif
			return &obj;
		}
		
	}
	
	return nullptr;
}

vm_prot_t CVMStuff::setProtection(uintptr_t address, size_t size, vm_prot_t prot) {
	mach_vm_address_t m_start = address;
	mach_vm_size_t m_size = size;
	natural_t m_depth = 0;
	kern_return_t krt;
	
	RegionInfo m_data;
	mach_msg_type_number_t info_size = kRegionInfoSize;
	
	krt = ::mach_vm_region_recurse(mach_task_self_, &m_start, &m_size, &m_depth, (vm_region_recurse_info_t)&m_data, &info_size);
	
	vm_prot_t current_prot = m_data.protection;
	
	auto pagesize = getpagesize();
	
#if DEBUG
	printf("[DEBUG] PAGESIZE == %i :: \n", pagesize);
#endif
	
	#if DEBUG
		printf("[DEBUG] Try to set new protection at == %p :: %#03x --> %#03x :::: Size >>> [%lx]\n", address, current_prot, prot, 4096);
	#endif
	
	if ((current_prot^prot) == 0) {
		#if DEBUG
				printf("[DEBUG] No need to set protection, aborting...\n");
		#endif
		return current_prot;
	}

	krt = ::mach_vm_protect(mach_task_self(), m_start, 4096, 0, prot);
	if (krt != KERN_SUCCESS) {
#if DEBUG
		printf("[DEBUG] Error setting new protection (%i) !\n", krt);
#endif
		return current_prot;
	}
	
#if DEBUG
	printf("[DEBUG] New protection successfully applied ! !\n");
#endif
	return current_prot;
}


uint64_t CVMStuff::patchAddressWithJMP(uintptr_t addressOfTarget, uintptr_t addressOfShim) {
	uint64_t datOrigCode = *reinterpret_cast<uint64_t*>(addressOfTarget);
	
	#if DEBUG
		cout << "[DEBUG] Start patching address = " << addressOfTarget << " :: content == " << hex << datOrigCode << endl;
	#endif
	
	mprotect((void*)addressOfTarget, 4096, VM_PROT_ALL);
	
	vm_prot_t current_prot = CVMStuff::setProtection(addressOfTarget, 4096, VM_PROT_READ | VM_PROT_WRITE | VM_PROT_COPY);
    vm_prot_t current_prot2 = CVMStuff::setProtection(addressOfTarget, 4096, VM_PROT_ALL);
	vm_prot_t current_prot3 = CVMStuff::setProtection(addressOfTarget, 4096, VM_PROT_ALL);


	uint32_t addressOffset = (uint32_t)((char*)addressOfShim - (char*)addressOfTarget - 5);
	addressOffset = OSSwapInt32(addressOffset);
	uint64_t jumpRelativeInstruction = 0xE900000000000000LL;
	jumpRelativeInstruction |= ((uint64_t)addressOffset & 0xffffffff) << 24;
	jumpRelativeInstruction = OSSwapInt64(jumpRelativeInstruction);

	*(uintptr_t*)addressOfTarget = jumpRelativeInstruction;

	kern_return_t krt = :: mach_vm_write(mach_task_self(), addressOfTarget, jumpRelativeInstruction, sizeof(uint64_t));
//
	#if DEBUG
		cout << "[DEBUG] No crash after JMP write, (macOS Big Sur testing) ( " << krt << " )" << endl;
	#endif
	
	#if DEBUG
		uint64_t newcode = *reinterpret_cast<uint64_t*>(addressOfTarget);
		cout << "[DEBUG] whats the value = " << reinterpret_cast<void*>(addressOfTarget) << " :: NEW content == " << hex << newcode << endl;
	#endif
	// if (krt != KERN_SUCCESS) {
	// 	#if DEBUG
	// 			printf("[DEBUG] Error writing (%i) !\n", krt);
	// 	#endif
	// }
	


	return datOrigCode;
}




uint64_t CVMStuff::patchAddressWithShimAddress(uintptr_t addressOfTarget, uintptr_t addressOfShim) {
	uint64_t datOrigCode = *reinterpret_cast<uint64_t*>(addressOfTarget);
	
	#if DEBUG
		cout << "[DEBUG] Start patching address = " << addressOfTarget << " :: content == " << hex << datOrigCode << endl;
	#endif
	
	kern_return_t krt = :: mach_vm_write(mach_task_self(), addressOfTarget, (uint64_t)&addressOfShim, sizeof(uint64_t));
	
	if (krt != KERN_SUCCESS) {
		#if DEBUG
			uint64_t newcode = *reinterpret_cast<uint64_t*>(addressOfTarget);
			cout << GREEN << "[DEBUG] Success patching address with Shim address, content = " << hex << newcode << RESET << endl;
		#endif
	} else {
		#if DEBUG
			uint64_t newcode = *reinterpret_cast<uint64_t*>(addressOfTarget);
			cout << RED << "[DEBUG] Error patching address with Shim address, content = " << hex << newcode << RESET << endl;
		#endif
	}


	return datOrigCode;
}








//void CVMStuff::addJMP(uint64_t* pAddressOfTarget, uint64_t* pAddressOfShim, string name) {
//	auto withASLR = CVMStuff::getASLRaddress((uint64_t)pAddressOfTarget);
//	data_to_jmp.push_back({name, false, (uint64_t*)withASLR , pAddressOfShim, NULL});
//
//#if DEBUG
//	printf("[DEBUG] Added JMP data = %s\n", name.c_str());
//#endif
//}
//



void CVMStuff::patchData() {
#if DEBUG
	printf("[DEBUG] Trying to patch data..\n");
#endif
	kern_return_t krt;
	
	for (Patch temp_patch: patches) {
#if DEBUG
		printf("[DEBUG] Patching at == %s\n", temp_patch.data_to_find.c_str());
#endif
		for (mach_vm_address_t add: temp_patch.addresses) {
			printf("setting protection\n");
			printf(" patching data at addr == %p\n", add);
			
			//mprotect((void*)add, 4096, PROT_READ | VM_PROT_COPY);


			// vm_prot_t current_prot = CVMStuff::setProtection((mach_vm_address_t*)add, temp_patch.data_to_pach.size(), VM_PROT_ALL);
			// vm_prot_t current_prot2 = CVMStuff::setProtection((mach_vm_address_t*)add, temp_patch.data_to_pach.size(), VM_PROT_ALL);
			vm_prot_t current_prot = CVMStuff::setProtection(add, 4096, VM_PROT_READ | VM_PROT_WRITE | VM_PROT_COPY);
			//*(uintptr_t*)add = 0x00000000;
			uint64_t newcode = *reinterpret_cast<uint64_t*>(add);
			cout << GREEN << "[DEBUG] Success patching address with Shim address, content = " << hex << newcode << RESET << endl;

			krt = :: mach_vm_write(mach_task_self_, add, (mach_vm_address_t)temp_patch.data_to_pach.data(), temp_patch.data_to_pach.size());
			uint64_t newcode2 = *reinterpret_cast<uint64_t*>(add);
			cout << GREEN << "[DEBUG] Success patching address with Shim address, content = " << hex << newcode2 << RESET << endl;

			
		}
	}
}

vm_prot_t CVMStuff::setProtection(const void *address, size_t size, vm_prot_t prot) {
	mach_vm_address_t m_start = (uintptr_t)address;
	mach_vm_size_t m_size = size;
	natural_t m_depth = 1024;
	kern_return_t krt;

	RegionInfo m_data;
	mach_msg_type_number_t info_size = kRegionInfoSize;

	krt = ::mach_vm_region_recurse(mach_task_self_, &m_start, &m_size, &m_depth, (vm_region_recurse_info_t)&m_data, &info_size);

	vm_prot_t current_prot = m_data.protection;

	#if DEBUG
		printf("[DEBUG] Try to set new protection at == %p :: %#03x --> %#03x :::: Size >>> [%lx]\n", address, current_prot, prot, size);
	#endif

	if ((current_prot^prot) == 0) {
		#if DEBUG
				printf("[DEBUG] No need to set protection, aborting...\n");
		#endif
		return current_prot;
	}

	krt = ::mach_vm_protect(mach_task_self_, m_start, size, 0, prot);
	if (krt != KERN_SUCCESS) {
		#if DEBUG
				printf("[DEBUG] Error setting new protection !\n");
		#endif
		return current_prot;
	}

	#if DEBUG
		printf("[DEBUG] New protection successfully applied ! !\n");
	#endif
	return current_prot;
}



void CVMStuff::addSectionInfo(string name, string segment, string section) {
	binary_sections.emplace_back(name, segment, section, NULL, 0, 0);
	
#if DEBUG
	printf("[DEBUG] Added section = <%s> :: segment <%s> to --> %s\n", segment.c_str(), section.c_str(), name.c_str());
#endif
}

// LEGACY version
// uint64_t CVMStuff::patchAddressWithJMP(uint64_t* pAddressOfTarget, uint64_t* pAddressOfShim, bool assembly) {
// 	uint64_t datOrigCode = *pAddressOfTarget;
// 	#if DEBUG
// 		printf("[DEBUG] Start patching address = %p with content == %llx \n", pAddressOfTarget, datOrigCode);
// 	#endif
	
// 	vm_prot_t current_prot = CVMStuff::setProtection((mach_vm_address_t*)pAddressOfTarget, sizeof(uint64_t), VM_PROT_ALL);
	
// 	if (assembly) {
// 		uint32_t addressOffset = (uint32_t)((char*)pAddressOfShim - (char*)pAddressOfTarget - 5);
// 		addressOffset = OSSwapInt32(addressOffset);
// 		uint64_t jumpRelativeInstruction = 0xE900000000000000LL;
// 		jumpRelativeInstruction |= ((uint64_t)addressOffset & 0xffffffff) << 24;
// 		jumpRelativeInstruction = OSSwapInt64(jumpRelativeInstruction);
		
// 		#if DEBUG
// 			printf("[DEBUG] Replaced = %llx with == %llx [assembly]\n", OSSwapInt64(datOrigCode), OSSwapInt64(jumpRelativeInstruction));
// 		#endif
		
// 		*pAddressOfTarget = jumpRelativeInstruction;
// 	} else {
// 		#if DEBUG
// 			printf("[DEBUG] Replaced = %llx with == %llx \n", datOrigCode, (uint64_t)pAddressOfShim);
// 		#endif
// 		*pAddressOfTarget = (uint64_t)pAddressOfShim;
// 	}
	
// 	CVMStuff::setProtection((mach_vm_address_t*)pAddressOfTarget, sizeof(uint64_t), current_prot);

// 	return datOrigCode;
// }


uint64_t CVMStuff::getASLRaddress(uint64_t addr) {
	// Here we need to implement support for more image not only the main one
	return addr + _dyld_get_image_vmaddr_slide(0);
}


jumper* CVMStuff::getJMPWithName(string name) {
	#if DEBUG
		printf("[DEBUG] Looking for a JMP-Data with name = %s\n", name.c_str());
	#endif
	
	
	//std::vector<std::unique_ptr<jumper>>::iterator ita;
	//auto it = find_if(data_to_jmp.begin(), data_to_jmp.end(), [name](const unique_ptr<jumper>& object) -> bool {return object->name == name; });
	//auto it = std::find_if(data_to_jmp.begin(), data_to_jmp.end(),  [name](const jumper& f){ return f.name == name; } );
	
	for(auto& obj : data_to_jmp) {
		if (obj.name == name) {
			#if DEBUG
				printf("[DEBUG] Found JMP-Data with name = %s , staus = %s\n", name.c_str(), obj.enabled ? "true" : "false");
			#endif
			return &obj;
		}
		
	}
	
	return &data_to_jmp[0];

	//return *it;
}
