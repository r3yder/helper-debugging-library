//
//  StartMeUp.cpp
//  CVM Projecter
//
//  Created by Marek Kulik on 07.33.2033.
//  Copyright © 2069 Marek Kulik. All rights reserved.
//

#include "StartMeUp.hpp"

#include "CVMStuff.hpp"
#include <future>


CVMStuff *the_god;
uint64_t aslr;
uint64_t* strlen_ptr;
uint64_t* bzero_ptr;
uint64_t* arc4_ptr;
uint64_t dyld_stub_binder_addr;
uint64_t counter_arc4 = 0;
uint64_t counter_strlen = 0;
uint64_t counter_bzero = 0;

void lets_go(void) __attribute__ ((constructor));

void library_validation_fake_stage_2() {
	the_god->getJMPWithName("library")->tap();
}

void library_validation_fake() {
	SLAP_STACK_FRAME
	PUSH_REGISTERS
	asm("call *%0": : "r" (&library_validation_fake_stage_2));
	POP_REGISTERS
	asm("ret");
}

void arc4_stub_fake_stage2() {
	the_god->getJMPWithName("arc4_stub")->tap();
	auto arc4_lb = the_god->getJMPWithName("arc4_lb");
	arc4_lb->tap(false);
	arc4_ptr = (uint64_t*)arc4_lb->datOrigCode;
}

void bzero_stub_fake_stage2() {
	the_god->getJMPWithName("bzero_stub")->tap();
	auto unk_lb = the_god->getJMPWithName("bzero_lb");
	unk_lb->tap(false);
	bzero_ptr = (uint64_t*)unk_lb->datOrigCode;
}

void strlen_stub_fake_stage2() {
	the_god->getJMPWithName("strlen_stub")->tap();
	auto strlen_lb = the_god->getJMPWithName("strlen_lb");
	strlen_lb->tap(false);
	strlen_ptr = (uint64_t*)strlen_lb->datOrigCode;
}

void arc4random_ln_fake() {
	SLAP_STACK_FRAME
	
	asm(".patch_arc4:");
	asm("pop %r10");			// This is here to remove return affress from arc4random from stack
	asm("add $0x108, %rsp");
	asm("pop %rbx");
	asm("pop %r12");
	asm("pop %r13");
	asm("pop %r14");
	asm("pop %r15");
	asm("pop %rbp");
	asm("ret");
}

void arc4random_stub_fake() {
	SLAP_STACK_FRAME
	
	asm("pop %r10");
	asm("push %r10");
	
	asm("cmp %%r10, %0;"
		"je .patch_arc4;"
		:
		: "r" (PATCH_ARC4_RETURN_STACK + aslr)
	);
	
	
	counter_arc4++;
	asm("cmp $0x2, %rax");
	asm("je .make_me_arc4_better");
	
	asm("push $0x3a0d;"
		"jmp *%0" : : "r" (PATCHES_JMP + aslr));
	
	asm(".make_me_arc4_better:");
	PUSH_REGISTERS
	asm("call *%0": : "r" (&arc4_stub_fake_stage2));
	POP_REGISTERS
	asm("jmp *%0" : : "r" (&arc4random_ln_fake));
}

void strlen_ln_fake() {
	SLAP_STACK_FRAME

	asm("pop %r10");
	asm("push %r10");

	asm("cmp %%r10, %0;"
		"je .patch_nr_1_stage_1;"
		:
		: "r" (PATCH_NR1_RETURN_STACK + aslr)
	);

	asm("cmp %%r10, %0;"
		"je .patch_nr_1_stage_1;"
		:
		: "r" (PATCH_NR2_RETURN_STACK + aslr)
	);


	asm("jmp .restore");


	asm(".patch_nr_1_stage_1:");
	asm("pop %r10");
	asm("add $0x80, %rsp");
	asm("pop %rbx");
	asm("pop %r14");
	asm("pop %rbp");
	asm("xor %rax, %rax;");
	asm("ret");



	asm(".restore:");
	asm("jmp *%0" : : "r" (strlen_ptr));

}

void strlen_stub_fake() {
	SLAP_STACK_FRAME

	asm("pop %r10");
	asm("push %r10");

	asm("cmp %%r10, %0;"
		"je .patch_nr_1_stage_1;"
		:
		: "r" (PATCH_NR1_RETURN_STACK + aslr)
	);

	asm("cmp %%r10, %0;"
		"je .patch_nr_1_stage_1;"
		:
		: "r" (PATCH_NR2_RETURN_STACK + aslr)
	);


	counter_strlen++;
	asm("cmp $0x2, %rax");
	asm("je .make_me_better");


	asm("push $0x4303;"
		"jmp *%0" : : "r" (PATCHES_JMP + aslr));

	asm(".make_me_better:");
	PUSH_REGISTERS
	asm("call *%0": : "r" (&strlen_stub_fake_stage2));
	POP_REGISTERS
	asm("jmp *%0" : : "r" (&strlen_ln_fake));
}


void bzero_ln_fake() {
	SLAP_STACK_FRAME

	asm("pop %r10");
	asm("push %r10");

	asm("cmp %%r10, %0;"
		"je .patch_nr_3;"
		:
		: "r" (PATCH_NR3_RETURN_STACK + aslr)
		);


	asm("jmp *%0" : : "r" (bzero_ptr));


	asm(".patch_nr_3:");
	asm("pop %r10");
	asm("add $0x1838, %rsp");
	asm("pop %rbx");
	asm("pop %r12");
	asm("pop %r13");
	asm("pop %r14");
	asm("pop %r15");
	asm("pop %rbp");
	asm("mov $1, %rax;");
	asm("ret");
}


void bzero_stub_fake() {
	SLAP_STACK_FRAME

	asm("pop %r10");
	asm("push %r10");

	asm("cmp %%r10, %0;"
		"je .patch_nr_3;"
		:
		: "r" (PATCH_NR3_RETURN_STACK + aslr)
	);


	counter_bzero++;
	asm("cmp $0x2, %rax");				// we need to allow dynamic binder to resolve correct address
	asm("je .make_me_better_bzero");

	asm(".end_point5:");
	asm("push $0x3063;"
		"jmp *%0" : : "r" (PATCHES_JMP + aslr));

	asm(".make_me_better_bzero:");
	PUSH_REGISTERS
	asm("call *%0": : "r" (&bzero_stub_fake_stage2));
	POP_REGISTERS
	asm("jmp *%0" : : "r" (&bzero_ln_fake));
}

void nsload_fake() {
	// (Un)patch NSLOAD
	auto nsload_jmp = the_god->getJMPWithName("nsload");
	nsload_jmp->tap();

	// Decrypt TEXT section
	void (*orig)();
	orig = (void(*)())nsload_jmp->pAddressOfTarget;
	orig();

	//the_god->searchForPatches();
	//the_god->patchData();

	aslr = _dyld_get_image_vmaddr_slide(0);

	the_god->getJMPWithName("strlen_stub")->tap();
	the_god->getJMPWithName("bzero_stub")->tap();
	the_god->getJMPWithName("arc4_lb")->tap(false);
}

//void dyld_stub_binder_fake() {
//	SLAP_STACK_FRAME
//
//	asm("jmp *%0" : : "r" (&dyld_stub_binder_addr));
//}

void lets_go() {

	// OLD version code, LEGACY


	// Here are sections that we need and DYLIB needs to find
	// static const string section_types[3][3] = {
	// 	{ "MacOS/Binary_name", "HEADER", "HEADER" },
	// 	{ "MacOS/Binary_name", "__TEXT", "__text" },
	// 	{ "MacOS/Binary_name", "__DATA", "__la_symbol_ptr" },
	// };
	
	// Patch data
	// static const string patch_data[1][5] = {
	// 	{	// This is our framework patch. It's executed only once. We are not gonna use it. Instead we will use JMP to patch it. But let's keep it
	// 		"MacOS/Binary_name", "__TEXT", "__text",
	// 		"\x55\x48\x89\xE5\x41\x57\x41\x56\x41\x55\x41\x54\x53\x48\x81\xEC\x28\x01\x00\x00\x48\x89\xFB\xE8\x4C\x47\x00\x00"s,
	// 		"\xC3\x48\x89\xE5\x41\x57\x41\x56\x41\x55\x41\x54\x53\x48\x81\xEC\x28\x01\x00\x00\x48\x89\xFB\xE8\x4C\x47\x00\x00"s
	// 	}
	// };


	
	// Patch data
	static const string patch_data[][6] = {
		{   // Those are patches responsible for restoring oryginal HEADER in memory after run to make integrity checks pass.
			"RESTORE_HEADER_1",
			"MacOS/Binary_name", "__TEXT", "",
			"\x40\x1B\x00\x00\x85"s,
			"\x20\x1B\x00\x00\x85"s
		},
		{
			"RESTORE_HEADER_2",
			"MacOS/Binary_name", "__TEXT", "",
			"\x07\x00\x00\x00\x05\x00\x00\x00\x15"s,
			"\x05\x00\x00\x00\x05\x00\x00\x00\x15"s
		},
		{
			"RESTORE_HEADER_3",
			"MacOS/Binary_name", "__TEXT", "",
			"\x00\x40\x03\x00\x00\x00\x00\x00\x00\xC0\x86\x00\x00\x00\x00\x00\x50\x34\x03"s,
			"\x00\xC0\x04\x00\x00\x00\x00\x00\x00\xC0\x86\x00\x00\x00\x00\x00\xF0\x92\x04"s
		},
		{
			"RESTORE_HEADER_4",
			"MacOS/Binary_name", "__TEXT", "",
			"\x0C\x00\x00\x00\x30\x00\x00\x00\x18\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x40\x72\x70\x61\x74\x68\x2F\x6C\x69\x62\x48\x6F\x62\x62\x65\x72\x2E\x64\x79\x6C\x69\x62\x00\x00"s,
			"\x1D\x00\x00\x00\x10\x00\x00\x00\x50\xF4\x89\x00\xA0\x5E\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"s
		}
	};
	
	
	
	// Shim data
	static const tuple<string, string, uintptr_t, uintptr_t> shim_data[] = {
		{
			"arc4_stub",
			"MacOS/Binary_name",
			(uintptr_t)ARC4_STUB_ADDR,
			(uintptr_t)&arc4random_stub_fake
		},
		// Only one for testing
		// {
		// 	"strlen_stub",
		// 	"MacOS/Binary_name",
		// 	(uintptr_t)STRLEN_STUB_ADDR,
		// 	(uintptr_t)&strlen_stub_fake
		// },
		// {
		// 	"strlen_lb",
		// 	"MacOS/Binary_name",
		// 	(uintptr_t)STRLEN_LB_ADDR,
		// 	(uintptr_t)&strlen_ln_fake
		// },
		// {
		// 	"bzero_stub",
		// 	"MacOS/Binary_name",
		// 	(uintptr_t)BZERO_STUB_ADDR,
		// 	(uintptr_t)&bzero_stub_fake
		// },
		// {
		// 	"bzero_lb",
		// 	"MacOS/Binary_name",
		// 	(uintptr_t)BZERO_LB_ADDR,
		// 	(uintptr_t)&bzero_ln_fake
		// },
		// {
		// 	"arc4_stub",
		// 	"MacOS/Binary_name",
		// 	(uintptr_t)ARC4_STUB_ADDR,
		// 	(uintptr_t)&arc4random_stub_fake
		// },
		// {
		// 	"arc4_lb",
		// 	"MacOS/Binary_name",
		// 	(uintptr_t)ARC4_LB_ADDR,
		// 	(uintptr_t)&arc4random_ln_fake
		// },
		// {
		// 	"magick",
		// 	"MacOS/Binary_name",
		// 	(uintptr_t)MAGICK_SHIT,
		// 	(uintptr_t)&arc4random_ln_fake
		// }
	};
	
	// JMP data
	// OLD LEGACY code
	// vector<tuple<string,uint64_t*,uint64_t*>> jmp_data;
	// jmp_data.emplace_back("nsload", (uint64_t*)NS_OBJ_LOAD_ADDR, (uint64_t*)&nsload_fake);
	// jmp_data.emplace_back("strlen_stub", (uint64_t*)STRLEN_STUB_ADDR, (uint64_t*)&strlen_stub_fake);
	// jmp_data.emplace_back("strlen_lb", (uint64_t*)STRLEN_LB_ADDR, (uint64_t*)&strlen_ln_fake);
	// jmp_data.emplace_back("bzero_stub", (uint64_t*)BZERO_STUB_ADDR, (uint64_t*)&bzero_stub_fake);
	// jmp_data.emplace_back("bzero_lb", (uint64_t*)BZERO_LB_ADDR, (uint64_t*)&bzero_ln_fake);
	// jmp_data.emplace_back("arc4_stub", (uint64_t*)ARC4_STUB_ADDR, (uint64_t*)&arc4random_stub_fake);
	// jmp_data.emplace_back("arc4_lb", (uint64_t*)ARC4_LB_ADDR, (uint64_t*)&arc4random_ln_fake);
	
	
	aslr = _dyld_get_image_vmaddr_slide(0);
	
	// Create Patcher instance
	the_god = new CVMStuff();
	
	// Add patch data
	for (auto data : patch_data) {
		the_god->addPatch(data[0], data[4], data[5], patch_image_vars(data[1], data[2], data[3]));
	}
	
	// Add shim data
	for (auto data : shim_data) {
		auto &[name, image, offset, addr] = data;
		the_god->addShim(name, image, offset, addr);
	}
	
	// Look for sections
	the_god->lookForSections();
	
	// Search for shims
	the_god->searchForShims();

	// Search for patches
	the_god->searchForPatches();
		
	the_god->patchData();

	the_god->getJMPWithName("nsload")->tap();
}
